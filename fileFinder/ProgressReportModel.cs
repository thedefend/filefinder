﻿using System.Windows.Forms;

namespace fileFinder
{
    class ProgressReportModel
    {
        public int matchingFiles { get; set; }
        public int processedFiles { get; set; }
        public int totalNumOfFiles { get; set; }
        public string currentFileUrl { get; set; }
        public TreeNode currentTreeNode { get; set; }
    }
}
