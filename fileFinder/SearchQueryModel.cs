﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fileFinder
{
    class SearchQueryModel 
    {
        public string fileUrl { get; set; }
        public string fileNameQuery { get; set; }
        public string fileInnerQuery { get; set; }
    }
}
