﻿namespace fileFinder
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.resultViewer = new System.Windows.Forms.TreeView();
            this.dirSelectBtn = new System.Windows.Forms.Button();
            this.fileUrlTextBox = new System.Windows.Forms.TextBox();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.fileInnerQueryLabel = new System.Windows.Forms.Label();
            this.fileNameQueryLabel = new System.Windows.Forms.Label();
            this.fileUrlLabel = new System.Windows.Forms.Label();
            this.handleSearchBtn = new System.Windows.Forms.Button();
            this.innerQueryTextBox = new System.Windows.Forms.TextBox();
            this.nameQueryTextBox = new System.Windows.Forms.TextBox();
            this.waitOrPauseLabel = new System.Windows.Forms.Label();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.infoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultViewer
            // 
            this.resultViewer.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.resultViewer.Dock = System.Windows.Forms.DockStyle.Top;
            this.resultViewer.Location = new System.Drawing.Point(0, 0);
            this.resultViewer.Name = "resultViewer";
            this.resultViewer.Size = new System.Drawing.Size(800, 342);
            this.resultViewer.TabIndex = 6;
            // 
            // dirSelectBtn
            // 
            this.dirSelectBtn.Location = new System.Drawing.Point(668, 9);
            this.dirSelectBtn.Name = "dirSelectBtn";
            this.dirSelectBtn.Size = new System.Drawing.Size(120, 21);
            this.dirSelectBtn.TabIndex = 2;
            this.dirSelectBtn.Text = "Выбрать путь";
            this.dirSelectBtn.UseVisualStyleBackColor = true;
            this.dirSelectBtn.Click += new System.EventHandler(this.dirSelectBtn_Click);
            // 
            // fileUrlTextBox
            // 
            this.fileUrlTextBox.Location = new System.Drawing.Point(151, 10);
            this.fileUrlTextBox.Name = "fileUrlTextBox";
            this.fileUrlTextBox.Size = new System.Drawing.Size(511, 20);
            this.fileUrlTextBox.TabIndex = 1;
            this.fileUrlTextBox.Text = "C:\\Users\\itsmy_000\\Desktop\\a\\";
            this.fileUrlTextBox.TextChanged += new System.EventHandler(this.fileUrlTextBox_TextChanged);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.fileInnerQueryLabel);
            this.splitContainer.Panel1.Controls.Add(this.fileNameQueryLabel);
            this.splitContainer.Panel1.Controls.Add(this.fileUrlLabel);
            this.splitContainer.Panel1.Controls.Add(this.handleSearchBtn);
            this.splitContainer.Panel1.Controls.Add(this.dirSelectBtn);
            this.splitContainer.Panel1.Controls.Add(this.innerQueryTextBox);
            this.splitContainer.Panel1.Controls.Add(this.nameQueryTextBox);
            this.splitContainer.Panel1.Controls.Add(this.fileUrlTextBox);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.waitOrPauseLabel);
            this.splitContainer.Panel2.Controls.Add(this.resultViewer);
            this.splitContainer.Panel2.Controls.Add(this.bottomPanel);
            this.splitContainer.Size = new System.Drawing.Size(800, 450);
            this.splitContainer.SplitterDistance = 88;
            this.splitContainer.TabIndex = 4;
            // 
            // fileInnerQueryLabel
            // 
            this.fileInnerQueryLabel.AutoSize = true;
            this.fileInnerQueryLabel.Location = new System.Drawing.Point(12, 65);
            this.fileInnerQueryLabel.Name = "fileInnerQueryLabel";
            this.fileInnerQueryLabel.Size = new System.Drawing.Size(133, 13);
            this.fileInnerQueryLabel.TabIndex = 6;
            this.fileInnerQueryLabel.Text = "Искомый текст в файле:";
            // 
            // fileNameQueryLabel
            // 
            this.fileNameQueryLabel.AutoSize = true;
            this.fileNameQueryLabel.Location = new System.Drawing.Point(12, 39);
            this.fileNameQueryLabel.Name = "fileNameQueryLabel";
            this.fileNameQueryLabel.Size = new System.Drawing.Size(119, 13);
            this.fileNameQueryLabel.TabIndex = 6;
            this.fileNameQueryLabel.Text = "Шаблон имени файла:";
            // 
            // fileUrlLabel
            // 
            this.fileUrlLabel.AutoSize = true;
            this.fileUrlLabel.Location = new System.Drawing.Point(12, 13);
            this.fileUrlLabel.Name = "fileUrlLabel";
            this.fileUrlLabel.Size = new System.Drawing.Size(76, 13);
            this.fileUrlLabel.TabIndex = 6;
            this.fileUrlLabel.Text = "Путь к папке:";
            // 
            // handleSearchBtn
            // 
            this.handleSearchBtn.Location = new System.Drawing.Point(668, 61);
            this.handleSearchBtn.Name = "handleSearchBtn";
            this.handleSearchBtn.Size = new System.Drawing.Size(120, 21);
            this.handleSearchBtn.TabIndex = 5;
            this.handleSearchBtn.Text = "Запустить поиск";
            this.handleSearchBtn.UseVisualStyleBackColor = true;
            this.handleSearchBtn.Click += new System.EventHandler(this.handleSearchBtn_Click);
            // 
            // innerQueryTextBox
            // 
            this.innerQueryTextBox.Location = new System.Drawing.Point(151, 62);
            this.innerQueryTextBox.Name = "innerQueryTextBox";
            this.innerQueryTextBox.Size = new System.Drawing.Size(511, 20);
            this.innerQueryTextBox.TabIndex = 4;
            this.innerQueryTextBox.Tag = "";
            this.innerQueryTextBox.TextChanged += new System.EventHandler(this.innerQueryTextBox_TextChanged);
            // 
            // nameQueryTextBox
            // 
            this.nameQueryTextBox.Location = new System.Drawing.Point(151, 36);
            this.nameQueryTextBox.Name = "nameQueryTextBox";
            this.nameQueryTextBox.Size = new System.Drawing.Size(511, 20);
            this.nameQueryTextBox.TabIndex = 3;
            this.nameQueryTextBox.Tag = "";
            this.nameQueryTextBox.Text = "*.txt";
            this.nameQueryTextBox.TextChanged += new System.EventHandler(this.nameQueryTextBox_TextChanged);
            // 
            // waitOrPauseLabel
            // 
            this.waitOrPauseLabel.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.waitOrPauseLabel.Location = new System.Drawing.Point(203, 137);
            this.waitOrPauseLabel.Name = "waitOrPauseLabel";
            this.waitOrPauseLabel.Size = new System.Drawing.Size(395, 68);
            this.waitOrPauseLabel.TabIndex = 7;
            this.waitOrPauseLabel.Text = "Приложение выполняет поиск по заданным условиям. \r\nДля ознакомления с результатам" +
    "и нажмите кнопку паузы\r\nили дождитесь окончания операции.";
            this.waitOrPauseLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.waitOrPauseLabel.Visible = false;
            // 
            // bottomPanel
            // 
            this.bottomPanel.AutoSize = true;
            this.bottomPanel.Controls.Add(this.infoLabel);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 345);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(800, 13);
            this.bottomPanel.TabIndex = 4;
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Location = new System.Drawing.Point(0, 0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(301, 13);
            this.infoLabel.TabIndex = 0;
            this.infoLabel.Text = "Время: 00:00:00 | Кол-во файлов: 0/0/0 | Состояние: готов";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "fileFinder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView resultViewer;
        private System.Windows.Forms.Button dirSelectBtn;
        private System.Windows.Forms.TextBox fileUrlTextBox;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button handleSearchBtn;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.TextBox nameQueryTextBox;
        private System.Windows.Forms.TextBox innerQueryTextBox;
        private System.Windows.Forms.Label waitOrPauseLabel;
        private System.Windows.Forms.Label fileInnerQueryLabel;
        private System.Windows.Forms.Label fileNameQueryLabel;
        private System.Windows.Forms.Label fileUrlLabel;
    }
}

