﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace fileFinder
{
    public partial class MainForm : Form
    {
        TaskController mainController;
        SearchQueryModel searchQueryModel;
        System.Timers.Timer updateInfoTimer = new System.Timers.Timer(300);
        ProgressReportModel lastReport = null;
        bool isFieldsChanged = false;

        public MainForm()
        {
            InitializeComponent();
            mainController = new TaskController(this, resultViewer);
            this.updateInfoTimer.Elapsed += refreshInfoByTimer;
            this.updateInfoTimer.AutoReset = true;
            this.updateInfoTimer.Enabled = true;
            fillImagesToResultViewer();
            restoreLastSession();
        }

        private void restoreLastSession()
        {
            var sets = Properties.Settings.Default;
            fileUrlTextBox.Text = sets.fileUrl;
            innerQueryTextBox.Text = sets.fileInnerQuery;
            nameQueryTextBox.Text = sets.fileNameQuery;
            searchQueryModel = mainController.updateSearchQueryModel(sets.fileUrl,
                sets.fileInnerQuery, sets.fileNameQuery);
            isFieldsChanged = false;
        }

        private void fillImagesToResultViewer()
        {
            try
            {
                ImageList iconList = new ImageList();
                iconList.Images.Add(Properties.Resources.Folder);          // 0
                iconList.Images.Add(Properties.Resources.OpenedFolder);    // 1
                iconList.Images.Add(Properties.Resources.File);            // 2
                iconList.Images.Add(Properties.Resources.Search);          // 3
                resultViewer.ImageList = iconList;
                resultViewer.ImageIndex = 3;
                resultViewer.SelectedImageIndex = 3;
            }
            catch { }
        }

        private void dirSelectBtn_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(dialog.SelectedPath))
                {
                    fileUrlTextBox.Text = dialog.SelectedPath;
                }
            }
        }
    
        private void reportProgress(object sender, ProgressReportModel report)
        {
            if (report != null)
                lastReport = report;
        }

        private async void handleSearchBtn_Click(object sender, EventArgs e)
        {
            Progress<ProgressReportModel> progress = new Progress<ProgressReportModel>();
            progress.ProgressChanged += reportProgress;

            if (fileUrlTextBox.Text.Equals("") || nameQueryTextBox.Text.Equals("") || innerQueryTextBox.Text.Equals(""))
            {
                MessageBox.Show("Вы не заполнили одно из полей. Проверьте все и повторно нажмите \"Запуск задания\"!",
                    "Сообщение");
                return;
            }
            
            if (isFieldsChanged)                // если в форме изменился запрос
                mainController.stopTask();      // то принудительно завершаем задание
            isFieldsChanged = false;

            switch (mainController.state)
            {
                case TaskState.Created:
                    searchQueryModel = mainController.updateSearchQueryModel(fileUrlTextBox.Text,
                        nameQueryTextBox.Text, innerQueryTextBox.Text);
                    handleSearchBtn.Text = "Поставить на паузу";
                    resultViewer.Nodes.Clear();
                    mainController.beginTask();
                    await Task.Run(() => mainController.buildResultTree(searchQueryModel, progress));
                    if (mainController.state == TaskState.Finished)
                        handleSearchBtn.Text = "Запустить поиск";
                    break;
                case TaskState.Work:
                    handleSearchBtn.Text = "Продолжить";
                    mainController.pauseTask();
                    break;
                case TaskState.Pause:
                    handleSearchBtn.Text = "Поставить на паузу";
                    mainController.resumeTask();
                    break;
                case TaskState.Finished:
                    mainController = new TaskController(this, resultViewer);
                    goto case TaskState.Created;
            }
        }

        private void refreshInfoByTimer(object sender, ElapsedEventArgs e)
        {
            Action refresh = () =>
            {
                TimeSpan tS = mainController.elapsedTime();

                string fileNumCounters = lastReport.matchingFiles + "/" + lastReport.processedFiles 
                    + "/" + lastReport.totalNumOfFiles;

                string time = String.Format("{0:00}:{1:00}:{2:00}", tS.Hours, tS.Minutes, tS.Seconds);
                infoLabel.Text = "Время: " + time + " " + "| Кол-во файлов: " + fileNumCounters +
                    " | Статус: " + lastReport.currentFileUrl;

            };
            try { this.Invoke(refresh); } catch { }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.fileUrl = fileUrlTextBox.Text;
            Properties.Settings.Default.fileNameQuery = nameQueryTextBox.Text;
            Properties.Settings.Default.fileInnerQuery = innerQueryTextBox.Text;
            Properties.Settings.Default.Save();
        }

        private void fileUrlTextBox_TextChanged(object sender, EventArgs e)
        {
            prepareToNewTask();
        }

        private void nameQueryTextBox_TextChanged(object sender, EventArgs e)
        {
            prepareToNewTask();
        }

        private void innerQueryTextBox_TextChanged(object sender, EventArgs e)
        {
            prepareToNewTask();
        }

        private void prepareToNewTask()
        {
            if (!isFieldsChanged)
            {
                isFieldsChanged = true;
                handleSearchBtn.Text = "Начать поиск";
            }
        }
    }
}
